package VNWeb::User::Register;

use VNWeb::Prelude;


TUWF::get '/u/register', sub {
    return tuwf->resRedirect('/', 'temp') if auth;
    framework_ title => 'Register', sub {
        elm_ 'User.Register';
    };
};


elm_api UserRegister => undef, {
    username => { username => 1 },
    email    => { email => 1 },
    vns      => { int => 1 },
}, sub {
    my $data = shift;

    my $num = tuwf->dbVali("SELECT count FROM stats_cache WHERE section = 'vn'");
    return elm_Bot         if $data->{vns} < $num*0.995 || $data->{vns} > $num*1.005;
    return elm_Taken       if tuwf->dbVali('SELECT 1 FROM users WHERE username =', \$data->{username});
    return elm_DoubleEmail if tuwf->dbVali(select => sql_func user_emailexists => \$data->{email}, \undef);

    my $ip = tuwf->reqIP;
    my $id = tuwf->dbVali('INSERT INTO users', {
        username => $data->{username},
        mail     => $data->{email},
        ip       => $ip,
    }, 'RETURNING id');
    my(undef, $token) = auth->resetpass($data->{email});

    my $pass_reset_url = sprintf
    	"%s"
        , tuwf->reqBaseURI()."/u$id/setpass/$token";

    tuwf->resJSON({Registered => [$pass_reset_url]});
    tuwf->done;
    #elm_Registered
};

1;
